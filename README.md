# ndawo - Thola anything chief wami!

A bonafide [JAMstack](https://jamstack.org/best-practices/) app, built with best practices of modern front-end development in mind. Well...most of them anyway.


We'll be using Vue.js, vue-cli webpack template, SASS, Pugjs and consumes Foursquare API, especially public '/venues' endpoint. With that data,  you can search for cafes, pizza or bars(Yes, even places deemed uncouth) in any location you want. Results are shown in a grid interface and **your last ten recent searches are there for you to visit again (localStorage)**. When you click on one of the venues, application will take you to appropriate detail page where you can browse pictures of the place and tips by other users.


#note that many geolocation APIs including Foursqaure, Instagram, and all of the Google APIs have placed new and remarkablely tedious monetization 'strangleholds'. Because of this, the experience will not be as smooth as it could be

___
### Demo
https://goofy-volhard-66b400.netlify.com/


____
### Prerequisites

Running on a localhost is not recommended unless actively developing(refer to Demo link instead) if you must and if you insist, do use node v6.11.1 and use the ffg commands:

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:3003
yarn start

```

___
## Running the tests and linters

Because of time constraints, tests and code coverage tasks are not ready for this project. But project makes heavy use of linters (ESLint, Stylelint and as a formatter, Prettier). All linting tasks are ready for your use; they also check your styles and scripts on every pre-commit.

If you use an text-editor or an IDE that has support for extensions of the mentioned above libraries you can have the comfort of a very robust development environment :)

___
## Tidbits/ Technologies used and why.

1. Pug - Because it's much cleaner and prettier to look at. Inituitive for python devs!
2. Sass - for everyone who hates writing css to the core
3. VueJS - For all the developers who like things that are well documented with a boatload of active developers.
4. NodeJS - Well, because Node runs the web today and it is a dependecy in most JS frameworks.
5. Webpack - Because it comes with the Vue template when setting up. It also runs our dev server and builds our "/dist" for deployment into live servers on Netlify.com or Surge.sh
6. TravisCI - Continuous deployment is the way software was meant to be built and deployed.
7. Netlify/Surge.sh - [Netlify](https://netlify.com) is brilliant for pushing things into production and well suited for JAMstack apps. Even includes a code deployment pipeline. [Surge.sh](https://surge.sh) is a godsend for quick deploys as it uses the terminal instead of a web UI like Netlify.


___
## Security
The issues there are quality and the very obvious privacy issues. While developing this, the idea of security and safe nature of the internet became all to apparent. Scenario: As a dev, I decide to use Twitter's media API or Instagrams. They have live feeds of data that I could use and everytime you search for a place, which is not tied down to a known user and basically returns results just based on location and a few search string params. I may be over-thinking it, but I have seen one too many twerking videos on both these platforms, and God-fearing people looking for a restaurant in Musgrave would not appreciate QueenTwerk's day job being on display at a particular establishment meant for family dining. The only hope of filtering out such results would be flags from the API provider but many will slip through the cracks. Therefore I have opted not to use Social Media Public APIs even though from a media standpoint they would be an order of magintude more effective than any other API provider.

___
## Conclusion
In version 0.0.2 of this application, which I will make available on Github, I attempted to use Instagram's API which uses a geo-tagged media endpoint to return images from a location marked with LATLNG coords. These params, along with a string of a location name, are returned from an ealier call (a promise in ES6) made to the geolocating Google Maps/Places API. This is good and bad. Bad because Google wants my Credit card information to continue using the APIs as they deem me an excessive user. Good because I now am very cautious of writing code that makes to many calls to any single API without limitation. Google rate limiting tendencies rendered the Places and Maps too time consuming on an already tight deadline and thus removed in short order.

I will revisit the idea in my own time however, as I have seen some interesting tidbits of the Instagram API and possibilities of using, I dare say, Video instead of pictures, more easpecially since Instagram now has accesss to Facebook mind boggling petabytes of data, geolocated and stored/archived by FACEBOOK. It would add a nice dimension to see actuall videos of a place, like a crowd-sourced/ hive-minded recommendation engine with bare minimum code. That's only if I can safely get around the Security Concerns mentioned above, that would be a real big deal.

___
## Acknowledgements:

- Obielum Godson -  Node/Instagram API
- Amelia Power - VueJS/Axios
- VueJS Documentation
- Axios Documentation
- Quite a few articles on best practices, hence why asking browser was omitted from my code.

